// #include <stdint.h>

#include <stdio.h>
#include <stdlib.h>

#define ONE_MB (1024 * 1024)

/*
  determine malloc maximum

  assumptions:
    * at least one MiB available

  you should use `ulimit -v` to ensure this program does not eat your
  computer's RAM
*/
int main ()
{
  /* theoretical limit */
  // printf("One malloc can allocate at most %zu bytes.\n", SIZE_MAX);

  void *p = NULL; size_t s = ONE_MB, l, h;
  while ((p = malloc(s)) != NULL) { free(p); s *= 2; }
  h = s; s /= 2; l = s; s = (l + h) / 2;
  while (l < s && s < h) {
    if ((p = malloc(s)) != NULL) {
      free(p); l = s;
    } else
      h = s;
    s = (l + h) / 2;
  }
  printf("One malloc can allocate at most %zu bytes.\n", l);
  return 0;
}
