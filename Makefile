.PHONY: all clean

SHELL     = bash
PROGRAMS  = exercise1 exercise2 exercise3
MEM      ?= 1048576

CC        = gcc
CFLAGS    = -Wall -Wextra -Werror -std=c99

all: $(PROGRAMS)

run_ex1:
	ulimit -v $(MEM); ./exercise1

run_ex3:
	ulimit -v $(MEM); ./exercise3

clean:
	for p in $(PROGRAMS); do rm -f "$$p"{,.o}; done
