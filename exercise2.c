#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ONE_MB  (1024 * 1024)
#define S1      "0413496"
#define S2      "XXXXXXX"  /* b/c I'm all alone :-( */

/*
  make one of the printf's in main print both numbers by attacking the
  heap

  assumptions:
    * heap grows upwards
    * we find the student numbers w/in the first MiB from another
      malloc
*/
void heap_attack()
{
  char *p = malloc(1), *s1 = NULL, *s2 = NULL;
  for (size_t i = 0; i < ONE_MB; ++i, --p) {
    // printf("%c=%d,", *p, *p);
    if      (memcmp(p, S1, 8) == 0) s1 = p;
    else if (memcmp(p, S2, 8) == 0) s2 = p;
    if (s1 != NULL && s2 != NULL) break;
  }
  if (s1 == NULL || s2 == NULL) exit(1);
  if (s1 < s2)
    for (p = s1 + 7; p < s2; ++p) *p = ' ';
  else
    for (p = s2 + 7; p < s1; ++p) *p = ' ';
}

/*
  print student numbers w/ heap_attack
*/
int main()
{
  char *s1 = malloc(8);
  if (s1 == NULL) return -1;
  char *s2 = malloc(8);
  if (s2 == NULL) return -1;
  memcpy(s1, S1, 8);
  memcpy(s2, S2, 8);
  heap_attack();
  printf("student 1: %s\n", s1);
  printf("student 2: %s\n", s2);
  return 0;
}
